package com.covid.care.services;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.covid.care.models.LocationStats;

@Service
public class CoronaVirusDataService {
	
	@Autowired
	CovidReportFeignClient covidReportFeignClient;

    private List<LocationStats> allStats = new ArrayList<>();

    public List<LocationStats> getAllStats() {
        return allStats;
    }

    @PostConstruct
    @Scheduled(cron = "* * 1 * * *")
    public void fetchVirusData() throws IOException, InterruptedException {
        List<LocationStats> newStats = new ArrayList<>();
        String data=covidReportFeignClient.getCovidCareReportData();
        StringReader csvBodyReader = new StringReader(data);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvBodyReader);
		for (CSVRecord record : records) {
			
			LocationStats locationStat = new LocationStats();
			locationStat.setState(record.get("Province/State"));
			locationStat.setCountry(record.get("Country/Region"));
			Integer latestCases=0;
			Integer prevDayCases=0;
			if(record.get(record.size() - 1) !=null && !record.get(record.size() - 1).trim().isEmpty())
			{
			   latestCases = Integer.parseInt(record.get(record.size() - 1)); 
			}
			if(record.get(record.size() - 2) !=null && !record.get(record.size() - 2).trim().isEmpty())
			{
			   prevDayCases = Integer.parseInt(record.get(record.size() - 2));
			}
			locationStat.setLatestTotalCases(latestCases);
			locationStat.setDiffFromPrevDay(prevDayCases-latestCases);
			newStats.add(locationStat);
			 }
        this.allStats = newStats;
    }

}
