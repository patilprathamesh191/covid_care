package com.covid.care.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "covidReportFeignClient", url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/")
public interface CovidReportFeignClient {
	
	@RequestMapping(method = RequestMethod.GET, path = "/archived_data/archived_time_series/time_series_19-covid-Confirmed_archived_0325.csv")
    String getCovidCareReportData();

}
